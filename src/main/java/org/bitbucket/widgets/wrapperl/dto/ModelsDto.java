package org.bitbucket.widgets.wrapperl.dto;

import lombok.Data;

@Data
public class ModelsDto {
    private String resString = "hello";
    private int resultInt = 27;
}
